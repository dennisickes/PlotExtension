package de.horstblocks.plotextension;

import com.intellectualcrafters.plot.object.Plot;
import com.intellectualcrafters.plot.object.PlotBlock;
import de.horstblocks.plotextension.handler.border.BorderChangerCommand;
import de.horstblocks.plotextension.handler.border.BorderChangerListener;
import de.horstblocks.plotextension.unit.BorderArt;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class PlotExtension extends JavaPlugin {

    private static PlotExtension INSTANCE;

    @Override
    public void onEnable() {
        INSTANCE = this;

        Bukkit.getPluginManager().registerEvents(new BorderChangerListener(), this);

        getCommand("changeborder").setExecutor(new BorderChangerCommand());

        super.onEnable();
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public boolean changeBorder(Plot plot, BorderArt art) {
        return plot.setComponent("border", new PlotBlock[]{new PlotBlock((short) art.getItem().getType().getId(), art.getItem().getData().getData())});
    }

    public static PlotExtension getInstance() {
        return INSTANCE;
    }
}
