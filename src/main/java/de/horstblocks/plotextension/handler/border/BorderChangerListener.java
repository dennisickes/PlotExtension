package de.horstblocks.plotextension.handler.border;

import com.intellectualcrafters.plot.object.Plot;
import de.horstblocks.plotextension.PlotExtension;
import de.horstblocks.plotextension.unit.BorderArt;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class BorderChangerListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        if (e.getInventory().getName().equals("§aSuch dir ein Block aus")) {
            e.setCancelled(true);

            Player player = (Player) e.getWhoClicked();
            if (e.getCurrentItem().getType() == Material.AIR) {
                return;
            }

            if (!BorderChangerCommand.HASHED_PLOTS.containsKey(player)) {
                player.sendMessage("§cEtwas ist schief gelaufen, versuche es nochmal.");
                player.closeInventory();
                return;
            }

            if (BorderArt.valueOf(e.getCurrentItem()) == null) {
                player.sendMessage("§cEtwas ist schief gelaufen, versuche es nochmal.");
                player.closeInventory();
                return;
            }

            Plot plot = BorderChangerCommand.HASHED_PLOTS.get(player);
            BorderArt art = BorderArt.valueOf(e.getCurrentItem());

            if (!PlotExtension.getInstance().changeBorder(plot, art)) {
                player.sendMessage("§cEtwas ist schief gelaufen, versuche es nochmal.");
                player.closeInventory();
                return;
            }

            BorderChangerCommand.HASHED_PLOTS.remove(player);
            player.sendMessage("§aDer Rand vom Plot wurde erfolgreich geändert.");
            player.closeInventory();
        }
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Player player = (Player) e.getPlayer();

        if (e.getInventory().getName().equals("§aSuch dir ein Block aus")) {
            BorderChangerCommand.HASHED_PLOTS.remove(player);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player player = e.getPlayer();
        BorderChangerCommand.HASHED_PLOTS.remove(player);
    }

}
