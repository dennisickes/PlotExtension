package de.horstblocks.plotextension.handler.border;

import com.intellectualcrafters.plot.object.Plot;
import com.intellectualcrafters.plot.object.PlotPlayer;
import de.horstblocks.plotextension.unit.BorderArt;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashMap;

public class BorderChangerCommand implements CommandExecutor {

    public static HashMap<Player, Plot> HASHED_PLOTS = new HashMap<Player, Plot>();
    private final static Inventory border_inventory = Bukkit.createInventory(null, 9 * 3, "§aSuch dir ein Block aus");

    public BorderChangerCommand() {
        int i = 0;
        for (BorderArt art : BorderArt.values()) {
            border_inventory.setItem(i, art.getItem());
            i += 2;
        }
    }

    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

        if (!(cs instanceof Player)) {
            cs.sendMessage("§cDieser Befehl ist nur für Spieler ausführbar!");
            return true;
        }

        Player player = (Player)cs;
        if (!player.hasPermission("horstblocks.plotextension.change.border")) {
            cs.sendMessage("§cDu hast nicht die Berechtigung diesen Befehl auszuführen.");
            return true;
        }

        if (PlotPlayer.get(player.getName()).getCurrentPlot() == null) {
            player.sendMessage("§cUm die Border zu ändern, musst du dich auf dein Plot befinden.");
            return true;
        }

        Plot plot = PlotPlayer.get(player.getName()).getCurrentPlot();

        if (!plot.getOwners().contains(player.getUniqueId()) || !plot.getMembers().contains(player.getUniqueId())) {
            player.sendMessage("§cUm die Border zu ändern, musst du dich auf dein Plot befinden.");
            return true;
        }

        HASHED_PLOTS.put(player, plot);

        player.openInventory(border_inventory);
        return true;
    }
}
