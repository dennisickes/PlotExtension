package de.horstblocks.plotextension.unit;

import org.bukkit.inventory.ItemStack;

public enum BorderArt {

    STONE(new ItemStack(44, 1), "Stein"),
    SAND(new ItemStack(44, 1, (short) 1), "Sandstein"),
    COBBLE(new ItemStack(44, 1, (short) 3), "Cobblestone"),
    BRICKS(new ItemStack(44, 1, (short) 4), "Ziegelstein"),
    STONE_BRICKS(new ItemStack(44, 1, (short) 5), "Steinziegel"),
    NETHER_BRICKS(new ItemStack(44, 1, (short) 6), "Netherziegel"),
    QUARTZ(new ItemStack(44, 1, (short) 7), "Quartz"),
    OAK_WOOD(new ItemStack(126, 1), "Eichenholz"),
    SPRUCE_WOOD(new ItemStack(126, 1, (short) 1), "Fichtenholz"),
    BIRCH_WOOD(new ItemStack(126, 1, (short) 2), "Birkenholz"),
    JUNGLE_WOOD(new ItemStack(126, 1, (short) 3), "Tropenholz"),
    ACACIA_WOOD(new ItemStack(126, 1, (short) 4), "Akazienholz"),
    DARK_OAK_WOOD(new ItemStack(126, 1, (short) 5), "Schwarzeichenholz"),
    PURPUR(new ItemStack(205, 1), "Purpur");

    private ItemStack item;
    private String name;

    BorderArt(ItemStack item, String name) {
        this.item = item;
        this.name = name;
    }

    public ItemStack getItem() {
        return item;
    }

    public String getName() {
        return name;
    }

    public static BorderArt valueOf(ItemStack item) {
        for (BorderArt art : values()) {
            if (art.getItem().getType() == item.getType() && art.getItem().getData().getData() == item.getData().getData()) {
                return art;
            }
        }

        return null;
    }
}
